FROM tigefa/debian

# Labels for MicroBadger
# https://microbadger.com/labels
LABEL org.label-schema.name="Buster Desktop" \
      org.label-schema.description="Xubuntu Desktop edition" \
      org.label-schema.vcs-url="https://gitlab.com/tigefa/buster-desktop" \
      org.label-schema.vendor="Sugeng Tigefa" \
      org.label-schema.schema-version="1.0" \
      maintainer="tigefa@gmail.com"

# Always up to date
RUN apt-get update -yqq && apt-get dist-upgrade -yqq

# Install Xubuntu Desktop
RUN apt-get install -yqq task-indonesian-desktop
RUN apt-get install -yqq task-xfce-desktop
RUN apt-get install -yqq task-gnome-desktop
RUN apt-get install -yqq tightvncserver

# Iunstall utilities
RUN apt-get install -yqq gnome-system-monitor
RUN apt-get install -yqq gnome-usage
RUN apt-get install -yqq *tilix*
RUN apt-get install -yqq *powerline*
RUN apt-get install -yqq *numix*
RUN apt-get install -yqq wget curl netcat aria2 whois figlet git bzr subversion mercurial cvs p7zip p7zip-full zip unzip

RUN sudo ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
RUN sudo dpkg-reconfigure -f noninteractive tzdata
RUN ln -s /etc/profile.d/vte-2.91.sh /etc/profile.d/vte.sh

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
